<?php


namespace Ipol\Viadelivery\Via\Controller;


use DateTime;
use Ipol\Viadelivery\Api\Entity\Request\CreateOrder as RequestObj;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\Client;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\DeliveryInfo;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\OrderLine;
use Ipol\Viadelivery\Api\Entity\Request\Part\CreateOrder\OrderLineList;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Core\Order\Order;
use Ipol\Viadelivery\Via\Entity\CreateOrderResult as ResultObj;

/**
 * Class SendOrder
 * @package Ipol\Viadelivery\Via
 * @subpackage Controller
 */
class SendOrder extends AutomatedCommonRequest
{
    /**
     * @var Order
     */
    protected $order;

    /**
     * @param ResultObj $resultObj
     * @param Order $order
     */
    public function __construct(ResultObj $resultObj, Order $order)
    {
        $this->order = $order;
        $this->requestObj = new RequestObj();
        parent::__construct($resultObj);
    }

    /**
     * @return $this
     * converts Core order to request-object for Api
     */
    public function convert()
    {
        $order = $this->order;

        $buyer = new Client();
        $buyer->setName($order->getReceivers()->getFirst()->getFullName())
            ->setEmail($order->getReceivers()->getFirst()->getEmail())
            ->setPhone($order->getReceivers()->getFirst()->getPhone());

        $deliveryInfo = new DeliveryInfo();
        $deliveryInfo->setPrice($order->getPayment()->getDelivery()->getAmount())
            ->setOutlet($order->getField('pointUuid'))
            ->setErrors($order->getField('arErrors'))
            ->setShippingCompanyHandle($order->getField('shippingCompanyHandle'));

        $obProductList = new OrderLineList();
        $itemCollection = $order->getItems();
        while ($item = $itemCollection->getNext())
        {
            $obProduct = new OrderLine();
            $obProduct->setVat($item->getVatRate())
                ->setTitle($item->getName())
                ->setWeight($item->getWeight() / 1000) //Core gram -> API kg
                ->setFullSalePrice($item->getPrice()->getAmount())
                //->setTotalPrice(Money::multiply($item->getField('basePrice'), $item->getQuantity())->getAmount()) deprecated (or not)
                ->setQuantity($item->getQuantity())
                ->setFullTotalPrice(Money::multiply($item->getPrice(), $item->getQuantity())->getAmount())
                ->setProductId($item->getId())
                //->setVariantId($item->getArticul())
                ->setBarcode($item->getBarcode());
            if(!is_null($item->getWidth()) && !is_null($item->getHeight()) && !is_null($item->getLength())) {
                $obProduct->setDimensions(($item->getWidth() / 10).'x'.($item->getHeight() / 10).'x'.($item->getLength() / 10)); //Core mm -> API formatted string with cm;
            }

            $obProductList->add($obProduct);
        }

        $this->requestObj->setId($order->getLink())
            ->setClient($buyer)
            ->setNumber($order->getNumber())
            ->setItemsPrice($order->getPayment()->getGoods()->getAmount())
            ->setOrderLines($obProductList)
            ->setTotalPrice(Money::sum($order->getPayment()->getGoods(),
                $order->getPayment()->getDelivery())->getAmount())
            ->setDeliveryInfo($deliveryInfo)
            ->setDeliveryPrice($order->getPayment()->getDelivery()->getAmount())
            ->setCurrencyCode($order->getItems()->getFirst()->getPrice()->getCurrency())
            ->setFulfillmentStatus($order->getField('fulfillmentStatus'))
            ->setCmsVersion($this->cmsVersion)
            ->setModuleVersion($this->moduleVersion);
        if ($order->getPayment()->getPayed()->getAmount() > 0) {
            //businessProcess declares: if some payment was received and order goes to Via
            //then consider this order paid
            $this->requestObj->setFinancialStatus('paid');
            /**@var DateTime $datePaid*/
            if ($datePaid = $order->getPayment()->getField('datePaid')) {
                $this->requestObj->setPaidAt($datePaid->format('Y-m-d\TH:i:s.vP'));
            }
        } else {
            $this->requestObj->setFinancialStatus('pending');
        }

        return $this;
    }

}