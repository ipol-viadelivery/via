<?php


namespace Ipol\Viadelivery\Via\Controller;


use Ipol\Viadelivery\Api\Entity\Request\GetPointInfo as RequestObj;
use Ipol\Viadelivery\Core\Delivery\Shipment;
use Ipol\Viadelivery\Core\Entity\Money;
use Ipol\Viadelivery\Via\AppLevelException;
use Ipol\Viadelivery\Via\Entity\PointResult as ResultObj;

/**
 * Class GetPointInfo
 * @package Ipol\Viadelivery\Via
 * @subpackage Controller
 */
class GetPointInfo extends AutomatedCommonRequest
{
    /**
     * @var Shipment|null
     */
    protected $shipment;

    /**
     * @param ResultObj $resultObj
     * @param $pointId
     * @param Shipment|null $shipment
     */
    public function __construct(ResultObj $resultObj, $pointId, ?Shipment $shipment)
    {
        parent::__construct($resultObj);
        $this->shipment = $shipment;
        $this->requestObj = new RequestObj();
        $this->requestObj->setPointId($pointId);
    }

    /**
     * @return GetPointInfo
     * @throws AppLevelException
     */
    public function convert(): GetPointInfo
    {
        $shipment = $this->shipment;
        $requestObj = $this->getRequestObj();

        //can't set uid in constructor - SDK will be configured only after that
        $requestObj->setId($this->getSdk()->getUid());
        if (is_null($shipment)) {
            return $this;
        }

        if (is_a($shipment->getField('totalOrderCost'), Money::class)) {
            $orderPrice = $shipment->getField('totalOrderCost');
        } else {
            $orderPrice = $shipment->getCargoes()->getTotalPrice();
        }

        $dimensions = $shipment->getCargoes()->getTotalDimensions();
        $requestObj->setWeight($shipment->getCargoes()->getTotalWeight())
            ->setLength($dimensions['L'] / 10) //Core mm -> API cm
            ->setWidth($dimensions['W'] / 10) //Core mm -> API cm
            ->setHeight($dimensions['H'] / 10) //Core mm -> API cm
            ->setOrderPrice($orderPrice->getAmount());

        return $this;
    }

    public function getSelfHash(): string
    {
        return md5(parent::getSelfHash() . $this->requestObj->getPointId());
    }
}