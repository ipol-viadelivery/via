<?php


namespace Ipol\Viadelivery\Via\Entity;


use Exception;
use Ipol\Viadelivery\Api\Entity\Response\GetPointInfo;

/**
 * Class PointResult
 * @package Ipol\Viadelivery\Via
 * @subpackage Entity
 * @method GetPointInfo|null getResponse()
 */
class PointResult extends AbstractResult
{
    /**
     * @var string
     */
    protected static $STATUS_ACTIVE = 'ACTIVE';
    /**
     * @var string
     */
    protected static $POINT_DEACTIVATED_MSG = 'Point not active';
    /**
     * @var string
     */
    protected $id;
    /**
     * @var float
     */
    protected $price;
    /**
     * @var string|null
     */
    protected $maxDays;
    /**
     * @var string|null
     */
    protected $minDays;

    public function parseFields()
    {
        $pointInfo = $this->getResponse();
        if (!$pointInfo->getId() || !$pointInfo->getStatus() || is_null($pointInfo->getPrice())) {
            $this->setSuccess(false);
            $this->setError(new Exception('Response validation error Critical field not set.'));
            return $this;
        }
        if (!($pointInfo->getStatus() === self::$STATUS_ACTIVE)) {
            $this->setSuccess(false);
            $this->setError(new Exception(self::$POINT_DEACTIVATED_MSG));
            return $this;
        }

        $this->id = $pointInfo->getId();
        $this->maxDays = $pointInfo->getMaxDays();
        $this->minDays = $pointInfo->getMinDays();
        $this->price = $pointInfo->getPrice();
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getMaxDays(): string
    {
        return $this->maxDays;
    }

    /**
     * @return string
     */
    public function getMinDays(): string
    {
        return $this->minDays;
    }

}