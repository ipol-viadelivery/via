<?php


namespace Ipol\Viadelivery\Via\Entity;


use Ipol\Viadelivery\Api\Entity\Response\GetDeliveryInfoModeDistance;
use Ipol\Viadelivery\Api\Entity\Response\Part\GetDeliveryInfoModeDistance\Point;

/**
 * Class CalculatorResult
 * @package Ipol\Viadelivery\Via
 * @subpackage Entity
 * @method GetDeliveryInfoModeDistance getResponse
 */
class ClosestPointsByAddressResult extends AbstractResult
{
    /**
     * @var Point|null
     */
    protected $closest;
    /**
     * @var Point|null
     */
    protected $cheapest;
    /**
     * @var Point|null
     */
    protected $fastest;
    /**
     * @return void
     */
    public function parseFields()
    {
        $response = $this->getResponse();
        if ($response->getPointArray()->getQuantity()) {
            $response->getPointArray()->reset();
            $pointInfo = $response->getPointArray()->getFirst();
            $this->closest = $this->cheapest = $this->fastest = $pointInfo;
            do {
                if ($pointInfo->getDistance() < $this->closest->getDistance()) {
                    $this->closest = $pointInfo;
                }
                if ($pointInfo->getPrice() < $this->cheapest->getPrice()) {
                    $this->cheapest = $pointInfo;
                }
                if ($pointInfo->getMinDays() < $this->fastest->getMinDays()) {
                    $this->fastest = $pointInfo;
                }
                if (!$pointInfo->getFullAddress()) {
                    $this->fillFullAddress($pointInfo);
                }
            } while ($pointInfo = $response->getPointArray()->getNext());
            /*if ($this->fastest->getDistance() > 50000) { //km TODO: how to determine units - not clear for now
                $this->setSuccess(false); //Too far away. Consider no points in requested area.
            }*/
        } else {
            $this->setSuccess(false);
        }
    }

    /**
     * @return Point|null
     */
    public function getClosest(): ?Point
    {
        return $this->closest;
    }

    /**
     * @return Point|null
     */
    public function getCheapest(): ?Point
    {
        return $this->cheapest;
    }

    /**
     * @return Point|null
     */
    public function getFastest(): ?Point
    {
        return $this->fastest;
    }

    protected function fillFullAddress(Point $pointInfo): void
    {
        if ($pointInfo->getDescription()) {
            $pointInfo->setFullAddress($pointInfo->getDescription());
        } elseif ($pointInfo->getPartner()) {
            $pointInfo->setFullAddress($pointInfo->getPartner());
        }
    }

}